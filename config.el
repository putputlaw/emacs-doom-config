;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

;; Place your private configuration here! Remember, you do not need to run 'doom
;; sync' after modifying this file!

;; ＵＳＥＲ　ＩＮＴＥＲＦＡＣＥ
;;
(setq doom-theme 'doom-dark+
      doom-font (font-spec :family "Source Code Pro" :size 16 :weight 'normal)
      doom-big-font (font-spec :family "Source Code Pro" :size 32 :weight 'normal))

;; ＥＤＩＴＩＮＧ
;;
(require 'iso-transl)
(remove-hook 'text-mode-hook #'turn-on-auto-fill)
(setq evil-escape-key-sequence "fd"
      doom-localleader-key ","
      display-line-numbers-type 'relative)
(map! :n "C-M--" #'doom/decrease-font-size
      :n "C-M-+" #'doom/increase-font-size
      :v "s" #'evil-surround-region
      :v "S" #'evil-substitute)

(map! :leader
      (:prefix-map ("t" . "toggle")
        :desc "Truncate lines" "l" #'toggle-truncate-lines
        :desc "Line numbers" "n" #'doom/toggle-line-numbers))

;; General Keybinds
(map! :n "K" #'evil-make)

;; ＲＵＳＴ
;;
(map! :map rustic-mode-map
      :localleader
      :desc "format buffer" "=" #'rustic-format-buffer
      :desc "cargo check" "c" #'rustic-cargo-check)
(eval-after-load 'rustic
  '(define-key rustic-compilation-mode-map (kbd "h") nil))
(setq-default lsp-rust-server 'rust-analyzer)

;; ＬＡＴＥＸ
;;
(setq +latex-viewers '(zathura okular evince))
(setq auto-mode-alist (cons '("\\.tex$" . context-mode) auto-mode-alist))

;; ＯＲＧ　ＭＯＤＥ
;;
(map! :map org-mode-map
      :localleader
      "," #'org-set-tags-command
      "m" #'org-sparse-tree
      "N" #'org-toggle-narrow-to-subtree
      "q" #'org-switchb)

(eval-after-load 'ox '(require 'ox-koma-letter))
(setq org-reveal-root "http://cdn.jsdelivr.net/reveal.js/3.0.0/")
(eval-after-load 'ox '(require 'ox-reveal))

;; open arxiv:1234.56789 links
;; this requires https://gitlab.com/putputlaw/arxiv-open
(eval-after-load 'ol
  '(defun org-arxiv-command (url)
     (start-process "" nil "arxiv-open" url)))
(eval-after-load 'ol
  '(org-link-set-parameters "arxiv"
                            :follow #'org-arxiv-command
                            :export nil
                            :store nil))
;; org-ref
(setq org-latex-pdf-process (list "latexmk -shell-escape -bibtex -f -pdf %f"))
(eval-after-load 'org
  '(require 'org-ref))

;; koma article
(with-eval-after-load 'ox-latex
  (add-to-list 'org-latex-classes
               '("koma-article"
                 "\\documentclass{scrartcl}"
                 ("\\section{%s}" . "\\section*{%s}")
                 ("\\subsection{%s}" . "\\subsection*{%s}")
                 ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
                 ("\\paragraph{%s}" . "\\paragraph*{%s}")
                 ("\\subparagraph{%s}" . "\\subparagraph*{%s}")))
  (add-to-list 'org-latex-classes
               '("koma-report"
                 "\\documentclass{scrreprt}"
                 ("\\chapter{%s}" . "\\chapter*{%s}")
                 ("\\section{%s}" . "\\section*{%s}")
                 ("\\subsection{%s}" . "\\subsection*{%s}")
                 ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
                 ("\\paragraph{%s}" . "\\paragraph*{%s}")
                 ("\\subparagraph{%s}" . "\\subparagraph*{%s}"))))

;; ＰＤＦ　ＶＩＥＷ ＭＯＤＥ
;;
;; TODO: rebind C-u and C-d

;; Some functionality uses this to identify you, e.g. GPG configuration, email
;; clients, file templates and snippets.
(setq user-full-name "Long Huynh Huu"
      user-mail-address "long@huynhgia.de")

;; Doom exposes five (optional) variables for controlling fonts in Doom. Here
;; are the three important ones:
;;
;; + `doom-font'
;; + `doom-variable-pitch-font'
;; + `doom-big-font' -- used for `doom-big-font-mode'; use this for
;;   presentations or streaming.
;;
;; They all accept either a font-spec, font string ("Input Mono-12"), or xlfd
;; font string. You generally only need these two:
;; (setq doom-font (font-spec :family "monospace" :size 12 :weight 'semi-light)
;;       doom-variable-pitch-font (font-spec :family "sans" :size 13))

;; There are two ways to load a theme. Both assume the theme is installed and
;; available. You can either set `doom-theme' or manually load a theme with the
;; `load-theme' function. This is the default:
;;(setq doom-theme 'doom-one)

;; If you use `org' and don't want your org files in the default location below,
;; change `org-directory'. It must be set before org loads!
(setq org-directory "~/nc.putputlaw.de/Org/")

;; This determines the style of line numbers in effect. If set to `nil', line
;; numbers are disabled. For relative line numbers, set this to `relative'.
(setq display-line-numbers-type t)


;; E M A I L
(set-email-account! "0 long@huynhgia.de"
                    '((user-mail-address      . "long@huynhgia.de")
                      (smtpmail-smtp-user     . "long@huynhgia.de")
                      (mu4e-sent-folder       . "/long@huynhgia.de/Gesendete Objekte")
                      (mu4e-drafts-folder     . "/long@huynhgia.de/Entw&APw-rfe")
                      (mu4e-trash-folder      . "/long@huynhgia.de/Papierkorb")
                      (mu4e-refile-folder     . "/long@huynhgia.de/Archiv")
                      (mu4e-compose-signature . "---\nLong Huynh Huu")))

(set-email-account! "1 long.kauft.ein@huynhgia.de"
                    '((user-mail-address      . "long.kauft.ein@huynhgia.de")
                      (smtpmail-smtp-user     . "long.kauft.ein@huynhgia.de")
                      (mu4e-sent-folder       . "/long.kauft.ein@huynhgia.de/Gesendete Objekte")
                      (mu4e-drafts-folder     . "/long.kauft.ein@huynhgia.de/Entw&APw-rfe")
                      (mu4e-trash-folder      . "/long.kauft.ein@huynhgia.de/Papierkorb")
                      (mu4e-refile-folder     . "/long.kauft.ein@huynhgia.de/Archiv")
                      (mu4e-compose-signature . "---\nLong Huynh Huu")))

;; R S S
(map! :leader
      (:prefix-map ("o" . "open")
        :desc "elfeed" "n" #'=rss))

(map! :map elfeed-search-mode-map
      :localleader
      "," #'elfeed-update
      ":" #'elfeed-update-feed)

;; Here are some additional functions/macros that could help you configure Doom:
;;
;; - `load!' for loading external *.el files relative to this one
;; - `use-package!' for configuring packages
;; - `after!' for running code after a package has loaded
;; - `add-load-path!' for adding directories to the `load-path', relative to
;;   this file. Emacs searches the `load-path' when you load packages with
;;   `require' or `use-package'.
;; - `map!' for binding new keys
;;
;; To get information about any of these functions/macros, move the cursor over
;; the highlighted symbol at press 'K' (non-evil users must press 'C-c c k').
;; This will open documentation for it, including demos of how they are used.
;;
;; You can also try 'gd' (or 'C-c c d') to jump to their definition and see how
;; they are implemented.
